'use strict';

class Hamburger {
    constructor(size, stuffing) {
        try {
            if (!stuffing) {
                throw new HamburgerException('Нужно выбрать начинку!');
            }
            if (!size) {
                throw new HamburgerException('Нужно выбрать размер!');
            }

            this.size = size;
            this.stuffing = stuffing;
            this.allTopping = [];
        } catch (e) {
            console.log('Введенные Вами данные не коректнные! Проверьте еще раз!', e);
        }
    }

    addTopping(topping) {
        try {
            if (!topping) {
                throw new HamburgerException('Выберите хотя бы одну добавку!');
            } else {
                !(this.allTopping.includes(topping)) ? this.allTopping.push(topping) : this.allTopping;
            }
        } catch (e) {
            console.log('Error: что-то пошло не так', e);
        }
    }

    removeTopping(topping) {
        try {
            if (!this.allTopping.length) {
                throw new HamburgerException('Не добавлено ни одной добавки!');
            }
            if (!(this.allTopping.includes(topping))) {
                throw new HamburgerException('Нельзя удалить добавку, которой нету!');
            }
            else {
                this.allTopping = this.allTopping.filter(value => value !== topping);
            }
        } catch (e) {
            console.log("Error: внимательно введите значение!", e);
        }
    };

    /**
     * Узнать размер гамбургера
     */
    get hamburgerSize() {
        return this.size;
    }

    /**
     * Узнать начинку гамбургера
     */
    get hamburgerStuffing() {
        return this.stuffing;
    }

    /**
     * Узнать добавку гамбургера
     */
    get hamburgerToppings() {
        return this.allTopping;
    }

    /**
     * Узнать цену гамбургера
     */
    get calculatePrice() {
        return (this.constructor.SIZES[this.size].price + this.constructor.STUFFINGS[this.stuffing].price
            + this.allTopping.reduce((acc, value) => acc + this.constructor.TOPPINGS[value].price, 0));
    }

    /**
     * Узнать калории гамбургера
     */
    get calculateCalories() {
        return (this.constructor.SIZES[this.size].calories + this.constructor.STUFFINGS[this.stuffing].calories
            + this.allTopping.reduce((acc, value) => acc + this.constructor.TOPPINGS[value].calories, 0));
    }
}

function HamburgerException(message) {
    this.message = message;
    return this;
}

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.SIZES = {
    [Hamburger.SIZE_SMALL]: {
        price: 50,
        calories: 20
    },
    [Hamburger.SIZE_LARGE]: {
        price: 100,
        calories: 40
    }
};

Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.STUFFINGS = {
    [Hamburger.STUFFING_CHEESE]: {
        price: 10,
        calories: 20
    },
    [Hamburger.STUFFING_SALAD]: {
        price: 20,
        calories: 5
    },
    [Hamburger.STUFFING_POTATO]: {
        price: 15,
        calories: 10
    }
};

Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';
Hamburger.TOPPINGS = {
    [Hamburger.TOPPING_MAYO]: {
        price: 15,
        calories: 0
    },
    [Hamburger.TOPPING_SPICE]: {
        price: 20,
        calories: 5
    }
};

let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

console.log(hamburger);

console.log('Size:', hamburger.hamburgerSize);
console.log('Stuffing:', hamburger.hamburgerStuffing);
console.log('Price:', hamburger.calculatePrice);
console.log('Calories:', hamburger.calculateCalories);

hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger);
console.log('Toppings:', hamburger.hamburgerToppings);
console.log('Price with sauce:', hamburger.calculatePrice);
console.log('Calories with sauce:', hamburger.calculateCalories);

hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(hamburger);
