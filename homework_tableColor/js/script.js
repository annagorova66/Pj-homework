let table = document.createElement('table');
document.body.appendChild(table);

for (let i = 0; i < 30; i++) {
    let tr = document.createElement('tr');
    table.appendChild(tr);

    for (let j = 0; j < 30; j++) {
        let td = document.createElement('td');
        td.className = 'box';
        tr.appendChild(td);
    }
}

let elemTable = document.querySelector('table');

elemTable.onclick = function (event) {
    let target = event.target;

    if (target.tagName === 'TD') {
        target.classList.toggle('black-box')
    }

    event.stopPropagation();
};

function reverseColor() {
    table.classList.toggle('reverse');
}

document.body.addEventListener('click', reverseColor, false);
