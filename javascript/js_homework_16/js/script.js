let standard = document.getElementById('standard');
let ultimate = document.getElementById('ultimate');
let premium = document.getElementById('premium');
let btn_contact = document.getElementById('btn_contact');
let btn_standard = document.getElementById('btn_standard');
let btn_ultimate = document.getElementById('btn_ultimate');
let btn_premium = document.getElementById('btn_premium');
let tables = document.getElementById('tables');

let i;
let arrayElements = [standard, ultimate, premium, btn_contact, btn_standard, btn_ultimate, btn_premium];

let keyValue = {
    'color': 'silver',
    'backgroundColor': 'olive',
    'borderColor': 'teal'
};

function fontsColor() {
    for (i = 0; i < arrayElements.length; i++) {
        arrayElements[i].style.backgroundColor = (arrayElements[i].style.backgroundColor === keyValue.backgroundColor) ? '' : keyValue.backgroundColor;
        localStorage.setItem('bdColor', arrayElements[i].style.backgroundColor);
    }

    tables.style.borderColor = (tables.style.borderColor === keyValue.borderColor) ? '' : keyValue.borderColor;
    localStorage.setItem('borderColor', tables.style.borderColor);

}

let bdColor = localStorage.getItem('bdColor');
let borderColor = localStorage.getItem('borderColor');

for (i = 0; i < arrayElements.length; i++) {
    if (bdColor === keyValue.backgroundColor) {
        arrayElements[i].style.backgroundColor = keyValue.backgroundColor;
    }
}
if (borderColor === keyValue.borderColor) {
    tables.style.borderColor = keyValue.borderColor;
}

fontsUpdate.addEventListener('click', fontsColor);