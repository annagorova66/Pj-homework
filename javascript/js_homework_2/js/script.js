function isNumber(value) {
    while (isNaN(value) || !Number.isInteger(Number(value)) || Number(value) <= 1) {
        if (isNaN(Number(value))) {
            value = prompt("Введите число!");
            // continue;
            break;
        }
        if (Number(value) <= 1) {
            value = prompt("Введите число больше 1!");
            // continue;
            break;
        }
        if (!Number.isInteger(Number(value))) {
            value = prompt("Введите целое число!");
        }
    }
    return Number(value);
}



let m = prompt('Введите число m!');
m = isNumber(m);

let n = prompt('Введите число n!');
n = isNumber(n);

nextPrime:
    for (let i = Number(m); i <= Number(n); i++) {
        for (let j = 2; j < i; j++) {
            if (i % j == 0) continue nextPrime;
        }
        console.log(i);
    }