function zodiac_sign(){
    let yourBirthday = prompt('Введите дату рождения в формате dd.mm.yyyy!');

    let m = yourBirthday.split('.');

    let day1 = Number(m[0]);
    let month1 = Number(m[1]);
    let year1 = Number(m[2]);
    let birthday = new Date(year1, month1-1, day1);

    let day = birthday.getDate();
    let month = birthday.getMonth()+1;
    let year = birthday.getFullYear();

    let now = new Date();
    let old = ((now.getTime() - birthday.getTime())/(24*3600*365.25*1000))|0;

    alert('Вам ' + old + ' лет');

    let zodiac = [
        {name: 'Козерог',   monthValue: 1,  dayValue: 20},
        {name: 'Водолей',   monthValue: 2,  dayValue: 20},
        {name: 'Рыбы',      monthValue: 3,  dayValue: 20},
        {name: 'Овен',      monthValue: 4,  dayValue: 20},
        {name: 'Телец',     monthValue: 5,  dayValue: 20},
        {name: 'Близнецы',  monthValue: 6,  dayValue: 21},
        {name: 'Рак',       monthValue: 7,  dayValue: 22},
        {name: 'Лев',       monthValue: 8,  dayValue: 23},
        {name: 'Дева',      monthValue: 9,  dayValue: 23},
        {name: 'Весы',      monthValue: 10, dayValue: 23},
        {name: 'Скорпион',  monthValue: 11, dayValue: 22},
        {name: 'Стрелец',   monthValue: 12, dayValue: 21},
        {name: 'Козерог',   monthValue: 13, dayValue: 20},
    ];

    if (day > zodiac[month-1].dayValue){
        alert('Ваш знак зодиака - ' + zodiac[month].name);
    } else {
        alert('Ваш знак зодиака - ' + zodiac[month-1].name);
    }

    let china_goroskop = ['Крыса', 'Бык', 'Тигр', 'Кролик', 'Дракон', 'Змея', 'Лошадь', 'Коза', 'Обезьяна', 'Петух', 'Собака', 'Свинья'];
    let year_from = new Date(1900,0,1);
    let diff_year = ((birthday.getTime() - year_from.getTime())/(24*3600*365.25*1000))|0;

    let item = diff_year%12;
    alert('По китайському гороскопу Вы - ' + china_goroskop[item]);
}

zodiac_sign();