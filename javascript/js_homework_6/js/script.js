function createNewUser(){
    let name = prompt('Enter firstName');
    let surname = prompt('Enter lastName');

    let user = {};

    Object.defineProperty(user, 'firstName', {
        get: function () { return name; },
        set: function setFirstName(newName) { return name = newName; }
    });

    Object.defineProperty(user, 'lastName', {
        get: function () { return surname; },
        set: function setLastName(newSurname) { return surname = newSurname; }
    });

    user.getLogin = function(){
        return (user.firstName.slice(0,1) + user.lastName.slice(0,1)).toLowerCase();
    };
    alert(user.getLogin());

    alert(user.firstName+' '+user.lastName);

}

createNewUser();