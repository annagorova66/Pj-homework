$('#draw').on('click', function () {
    $('#draw').hide();

    $('body').append("<input class='size_circle' placeholder='Диаметр круга'>");
    $('body').append("<input class='color_circle' placeholder='Цвет круга'>");
    $('body').append("<button class='draw_circle'>Нарисовать</button>");

    $('.draw_circle').on('click', function () {
        let sizeCircle =  $('.size_circle').val();
        let colorCircle =  $('.color_circle').val();

        $('body').append(
            $('<div></div>')
                .css('width', sizeCircle)
                .css('height', sizeCircle)
                .css('background-color', colorCircle)
                .css('border-radius', '50%')
                .css('margin', '20px')
        )

    })
});
