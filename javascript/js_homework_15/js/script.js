let start = document.getElementById('start');
let pause = document.getElementById('pause');
let clear = document.getElementById('clear');


let min = document.getElementById('min');
let sec = document.getElementById('sec');
let millisec = document.getElementById('millisec');


let clocktimer, dateObj, t;
let readout = '';
let init = 0;

let milliseconds, seconds, minutes;

//функция для очистки поля
function ClearСlock() {
    pause.style.display = 'none';
    start.style.display = 'inline-block';
    clearTimeout(clocktimer);

    init = 0;
    // readout = '00:00.00';
    // document.watch.stopwatch.value = readout;
    min.innerHTML = '00';
    sec.innerHTML = '00';
    millisec.innerHTML = '00';
}

//функция для старта секундомера
function StartTIME() {
    start.style.display = 'none';
    pause.style.display = 'inline-block';


    let cdateObj = new Date();
    t = new Date(cdateObj - dateObj);
    milliseconds = t.getMilliseconds();
    seconds = t.getSeconds();
    minutes = t.getMinutes();

    if (milliseconds < 100) {
        milliseconds = '0' + milliseconds;
    }
    if (seconds < 10) {
        seconds = '0' + seconds;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }

    readout = minutes + ':' + seconds + '.' + milliseconds;
    // document.watch.stopwatch.value = readout;
    min.innerHTML = minutes;
    sec.innerHTML = seconds;
    millisec.innerHTML = milliseconds;

    clocktimer = setTimeout("StartTIME()", 10);

    // return readout;
}

//Функция запуска и остановки
function StartClock() {
    if (init === 0) {
        // ClearСlock();
        dateObj = new Date();
        StartTIME();
    }
    if (init === 1) {
        console.log(readout);
        console.log(readout.length);
        StartTIME();
    }
}

function StopClock() {
    pause.style.display = 'none';
    start.style.display = 'inline-block';

    clearTimeout(clocktimer);
    init = 1;
}

start.addEventListener('click', StartClock);
pause.addEventListener('click', StopClock);
clear.addEventListener('click', ClearСlock);

