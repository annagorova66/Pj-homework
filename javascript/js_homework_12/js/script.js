function viewParams() {
    let delete_btn = document.getElementById('draw');
    delete_btn.style.display = 'none';

    let size_circle = document.createElement('input');
    size_circle.className = 'size_circle';
    size_circle.placeholder = 'Диаметр круга';
    document.body.appendChild(size_circle);

    let color_circle = document.createElement('input');
    color_circle.className = 'color_circle';
    color_circle.placeholder = 'Цвет круга';
    document.body.appendChild(color_circle);

    let draw_circle = document.createElement('button');
    draw_circle.className = 'draw_circle';
    draw_circle.id = 'draw_circle';
    draw_circle.addEventListener('click', draw_circles);
    document.body.appendChild(draw_circle);
    draw_circle.innerText = 'Нарисовать';
}

function draw_circles() {
    let diameter_value = Number(document.getElementsByTagName('input')[0].value);
    let color_value = document.getElementsByTagName('input')[1].value;

    let canvas = document.createElement('canvas');
    canvas.className = 'canvas';
    document.body.appendChild(canvas);

    let obCanvas = canvas.getContext('2d');
    obCanvas.beginPath();
    obCanvas.arc(150, 75, (diameter_value / 2), 0, 2 * Math.PI, false);
    obCanvas.fillStyle = color_value;
    obCanvas.fill();

}
draw.addEventListener('click', viewParams);