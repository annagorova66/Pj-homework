function viewParams() {
    let delete_btn = document.getElementById('draw');
    delete_btn.style.display = 'none';

    let size_circle = document.createElement('input');
    size_circle.className = 'size_circle';
    size_circle.placeholder = 'Диаметр круга';
    document.body.appendChild(size_circle);

    let draw_circle = document.createElement('button');
    draw_circle.className = 'draw_circle';
    draw_circle.id = 'draw_circle';
    draw_circle.addEventListener('click', draw_circles);
    document.body.appendChild(draw_circle);
    draw_circle.innerText = 'Нарисовать';
}

function draw_circles() {
    let diameter_value = Number(document.getElementsByTagName('input')[0].value);

    for (let i = 0; i < 100; i++) {
        let canvas = document.createElement('canvas');
        canvas.className = 'canvas';
        canvas.style.width = 'calc(100% / 10)';
        document.body.appendChild(canvas);

        let obCanvas = canvas.getContext('2d');
        obCanvas.beginPath();
        obCanvas.arc(150, 75, (diameter_value / 2), 0, 2 * Math.PI, false);
        obCanvas.fillStyle = getRandomColor();
        obCanvas.fill();
    }

    let circles = document.getElementsByClassName('canvas');
    for (let i = 0; i < 100; i++) {
        circles[i].addEventListener('click', function () {
            for (let i = 0; i < 100; i++) {
                circles[i].style.display = 'none';
            }
        });
    }
}

function getRandomColor() {
    let letters = '0123456789ABCDEF';//#ffffff #000000
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    if (color === "#FFFFFF") {
        color = getRandomColor();
    }
    console.log(color);
    return color;
}

draw.addEventListener('click', viewParams);