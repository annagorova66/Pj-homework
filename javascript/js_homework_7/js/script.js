let users = [
    {name: "Ivan", surname: "Ivanov", gender: "male", age: 30},
    {name: "Anna", surname: "Ivanova", gender: "female", age: 22},
    {name: "Olga", surname: "Ivanova", gender: "female", age: 20},
    {name: "Vlad", surname: "Ivanov", gender: "male", age: 30},
    {name: "Max", surname: "Ivanova", gender: "female", age: 22},
    {name: "Oleg", surname: "Ivanova", gender: "female", age: 22},
    {name: "Vasya", surname: "Ivanov", gender: "male", age: 10},
    {name: "Vlad", surname: "Ivanov", gender: "male", age: 30}
];

let persons = [
    {name: "Elena", surname: "Ivanov", gender: "male", age: 38},
    {name: "Vlad", surname: "Ivanov", gender: "male", age: 30},
    {name: "Anna", surname: "Pertove", gender: "female", age: 27}
];

function excludeBy(array1, array2, property) {
    let new_user = [];
    let person_property = [];

    array2.forEach(function (item, j, array2) {
        person_property.push(item[property]);
    });

    let users_filter = array1.filter(function (i) {
        for (let j = 0; j < person_property.length; j++) {
            if (i[property] === person_property[j]) {
                i.p_status1 = 'false';
            } else {
                new_user.push(i);
            }
        }
        return new_user;

    });

    users_filter.forEach(function (item, j, users_filter) {
        if (item.p_status1 === 'false') {
            users_filter.splice(j, 1);
        }
    });
    console.log(users_filter);
}

excludeBy(users, persons, 'name');

