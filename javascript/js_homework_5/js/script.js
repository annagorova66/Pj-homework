let obj_person = {
    name: 'Katya',
    surname: 'Ivanova',
    age: 25,
    size: {
        top: 90,
        middle: 60,
        bottom: 90
    }
};

function clone(value) {
    let obj_clone = {};

    for (let key in value) {
        obj_clone[key] = value[key];
    }

    console.log(obj_person);
    console.log(obj_clone);
}

clone(obj_person);