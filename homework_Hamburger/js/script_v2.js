/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
let Hamburger = function (size, stuffing) {
    try {
        if (!size){
            throw new HamburgerException('HamburgerException: no size given')
        } else if (size === Hamburger.SIZE_SMALL || size === Hamburger.SIZE_LARGE){
            this.size = size;
        } else{
            throw  new HamburgerException('HamburgerException: invalid size');
        }

        if (!stuffing) {
            throw new HamburgerException('HamburgerException: no stuffing given');
        } else if (stuffing === Hamburger.STUFFING_CHEESE || stuffing === Hamburger.STUFFING_POTATO || stuffing === Hamburger.STUFFING_SALAD){
            this.stuffing = stuffing;
        } else{
            throw  new HamburgerException('HamburgerException: invalid stuffing');
        }

        this.allTopping = [];

    } catch (e) {
        console.log('Введенные Вами данные не коректнные! Проверьте еще раз!', e);
    }

};

//-------------------------------------------------------------------------------------------
Hamburger.SIZE_SMALL = {name: 'SIZE_SMALL', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {name: 'SIZE_LARGE', price: 100, calories: 40};

Hamburger.STUFFING_CHEESE = {name: 'STUFFING_CHEESE', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: 'STUFFING_SALAD', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: 'STUFFING_POTATO', price: 15, calories: 10};

Hamburger.TOPPING_MAYO = {name: 'TOPPING_MAYO', price: 15, calories: 0};
Hamburger.TOPPING_SPICE = {name: 'TOPPING_SPICE', price: 20, calories: 5};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {

    try {
        if (!topping) {
            throw new HamburgerException('Выберите хотя бы одну добавку!');
        } else {
            for (let key = 0; key < topping.length; key++) {
                if (!this.allTopping.includes(topping[key])) {
                    this.allTopping.push(topping[key]);
                } else {
                    throw new HamburgerException('HamburgerException: duplicate topping');
                }
            }
        }
    } catch (e) {
        console.log('Error: что-то пошло не так', e);
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {

    try {
        if (!this.allTopping.length) {
            throw new HamburgerException('Не добавлено ни одной добавки!');
        }
        for (let key = 0; key < this.allTopping.length; key++) {
            if (this.allTopping[key]['name'] === topping[0]['name']) {
                let a = this.allTopping.splice(key, 1);
            }
        }
    } catch (e) {
        console.log('что-то пошло не так', e);
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.allTopping;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    this.price = this.size['price'] + this.stuffing['price'];

    if (this.allTopping.length){
        for(let key = 0; key < this.allTopping.length; key++) {
            this.price += this.allTopping[key]['price'];
        }
    }
    return this.price;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    this.calories = this.size['calories'] + this.stuffing['calories'];

    if (this.allTopping.length){
        for(let key = 0; key < this.allTopping.length; key++){
            this.calories += this.allTopping[key]['calories'];
        }
    }
    return this.calories;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
    this.message = message;
    return this;
}




// маленький гамбургер с начинкой из сыра
let myBurger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log('маленький гамбургер с начинкой из сыра ', myBurger);

// добавка из майонеза
myBurger.addTopping([Hamburger.TOPPING_MAYO]);
console.log('добавка из майонеза ', myBurger);

// спросим сколько там калорий
console.log("Calories: %f", myBurger.calculateCalories());
// сколько стоит
console.log("Price: %f", myBurger.calculatePrice());
// я тут передумал и решил добавить еще приправу
myBurger.addTopping([Hamburger.TOPPING_SPICE]);
console.log('добавка из майонеза + приправа', myBurger);
// А сколько теперь стоит?
console.log("Price with sauce: %f", myBurger.calculatePrice());
console.log("Calories again: %f", myBurger.calculateCalories());


// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", myBurger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
myBurger.removeTopping([Hamburger.TOPPING_SPICE]);
console.log('Убрать добавку ', myBurger);

myBurger.addTopping([Hamburger.TOPPING_SPICE]);
console.log('добавка из майонеза + приправа2 ', myBurger);
console.log("Have %d toppings", myBurger.getToppings().length); // 1

console.log("Size hamburger's is", myBurger.getSize());
console.log("Stuffing hamburger's is", myBurger.getStuffing());
console.log(myBurger.getToppings());
